from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User


class Convoy(models.Model):
	user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True)
	name = models.CharField(max_length=60)
	latitude_destination = models.FloatField()
	longitude_destination = models.FloatField()
	status = models.IntegerField()
	
	def __unicode__(self):
		return self.name

class Convoyer(models.Model):
	user = models.ForeignKey(User, on_delete=models.CASCADE)
	convoy = models.ForeignKey(Convoy, related_name='convoyers')
	latitude = models.FloatField()
	longitude = models.FloatField()
	device_token = models.CharField(max_length=120)
	status = models.IntegerField()

	def __unicode__(self):
		return self.convoy.name + " - " + self.user.username

class Friend(models.Model):
	user1 = models.ForeignKey(User, on_delete=models.CASCADE, related_name='friend_of')
	user2 = models.ForeignKey(User, on_delete=models.CASCADE, related_name='friend_to')
	status = models.IntegerField()

	def __unicode__(self):
		return self.user1.username + " - " + self.user2.username