from django.contrib import admin

from models import Convoy, Friend, Convoyer


class ConvoyerAdmin(admin.ModelAdmin):
	pass

class ConvoyAdmin(admin.ModelAdmin):
	pass

class FriendAdmin(admin.ModelAdmin):
	pass

admin.site.register(Convoyer, ConvoyerAdmin)
admin.site.register(Convoy, ConvoyAdmin)
admin.site.register(Friend, FriendAdmin)
