from django.conf.urls import include, url
from views import ConvoyView, FriendView, FriendView2, SearchFriendsView, UsuariosView2, CurrentConvoyView, ConvoyersView, ConvoyerView, HistorialView

from rest_framework_jwt.views import obtain_jwt_token, verify_jwt_token, refresh_jwt_token
from django.contrib.auth.views import password_reset, password_reset_done, password_reset_confirm, password_reset_complete, login

from rest_framework.authtoken.views import obtain_auth_token

urlpatterns = [	
	url(r'^auth/', include('djoser.urls.authtoken')), 
	url(r'^usuarios/(?P<pk>[0-9]+)/$', UsuariosView2.as_view({'get': 'retrieve','put': 'update','patch': 'partial_update'})),
	url(r'^convoy/$', ConvoyView.as_view({'get': 'list','post': 'create'})),
	url(r'^convoy/(?P<pk>[0-9]+)/$', ConvoyView.as_view({'get': 'retrieve','put': 'update','patch': 'partial_update','delete': 'destroy'})),
	url(r'^convoy/me/$', CurrentConvoyView.as_view()),
	url(r'^convoy/me/$', CurrentConvoyView.as_view()),	
	url(r'^convoy/convoyers/$', ConvoyersView.as_view()),
	url(r'^convoyers/(?P<pk>[0-9]+)/$', ConvoyerView.as_view({'get': 'retrieve','put': 'update','patch': 'partial_update','delete': 'destroy'})),
	url(r'^convoyers/$', ConvoyerView.as_view({'get': 'list','post': 'create'})),
	url(r'^friends/$', FriendView2.as_view({'get': 'list'})),
	url(r'^friend/$', FriendView.as_view({'get': 'list','post': 'create'})),
	url(r'^friends/(?P<pk>[0-9]+)/$', FriendView.as_view({'get': 'retrieve','put': 'update','patch': 'partial_update','delete': 'destroy'})),
	url(r'^friends/search/$', SearchFriendsView.as_view()),	
	url(r'^historial/me/$', HistorialView.as_view()),
]

