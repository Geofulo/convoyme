import json
from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.renderers import JSONRenderer
from serializers import ConvoySerializer, FriendSerializer, FriendSerializer2, UserSerializer2, ConvoyerSerializer, ConvoyerSerializer2
from models import Convoy, Friend, Convoyer
from django.http import JsonResponse, HttpResponse
from django.contrib.auth.models import User
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt
from django.db.models import Q


class UsuariosView2(ModelViewSet):
	queryset = User.objects.all()
	serializer_class = UserSerializer2

class ConvoyView(ModelViewSet):
	permission_classes = (IsAuthenticated,)
	queryset = Convoy.objects.all()
	serializer_class = ConvoySerializer

class ConvoyerView(ModelViewSet):
	permission_classes = (IsAuthenticated,)
	queryset = Convoyer.objects.all()
	serializer_class = ConvoyerSerializer2

class FriendView(ModelViewSet):
	# permission_classes = (IsAuthenticated,)
	queryset = Friend.objects.all()
	serializer_class = FriendSerializer


class FriendView2(ModelViewSet):
	permission_classes = (IsAuthenticated,)
	serializer_class = FriendSerializer2

	def get_queryset(self):
		queryset = Friend.objects.all()
		userId = self.request.query_params.get('user_id', None)
		if userId is not None:
			queryset = queryset.filter(Q(user1=userId) | Q(user2=userId))
		return queryset


class SearchFriendsView(APIView):
	def get(self, request, format=None):
		username = self.request.query_params.get('username', None)
		userId = self.request.query_params.get('user_id', None)
		friends_list = Friend.objects.filter(user1=userId).values_list('user2', flat=True)
		friends_list2 = Friend.objects.filter(user2=userId).values_list('user1', flat=True)
		# usuarios = User.objects.filter(username__startswith=username, is_superuser=False, is_staff=False).filter(first_name__startswith=username, is_superuser=False, is_staff=False).filter(last_name__startswith=username, is_superuser=False, is_staff=False)
		usuarios = User.objects.filter(Q(username__startswith=username) | Q(first_name__startswith=username) | Q(last_name__startswith=username), is_superuser=False, is_staff=False).exclude(Q(id__in=friends_list) | Q(id__in=friends_list2))
		serializer = UserSerializer2(usuarios, many=True)
		data = JSONRenderer().render(serializer.data)
		return HttpResponse(data, content_type='application/json')

class CurrentConvoyView(APIView):
	def get(self, request, format=None):
		userId = self.request.query_params.get('user_id', None)
		convoy = Convoy.objects.filter(user=userId).filter(~Q(status=3))[0]
		# convoyer = Convoyer.objects.get(user=userId)
		# convoyer = Convoyer.objects.filter(user=userId).exclude(convoy__in=convoy)		

		# usuarios = User.objects.filter(username__startswith=username, is_superuser=False, is_staff=False).filter(first_name__startswith=username, is_superuser=False, is_staff=False).filter(last_name__startswith=username, is_superuser=False, is_staff=False)
		# usuarios = User.objects.filter(Q(username__startswith=username) | Q(first_name__startswith=username) | Q(last_name__startswith=username), is_superuser=False, is_staff=False).exclude(Q(id__in=friends_list) | Q(id__in=friends_list2))
		# serializer = UserSerializer2(usuarios, many=True)
		
		# serializer = ConvoySerializer(convoyer.convoy, many=False)

		# serializer = ConvoyerSerializer(convoyer, many=False)
		serializer = ConvoySerializer(convoy, many=False)
		data = JSONRenderer().render(serializer.data)
		return HttpResponse(data, content_type='application/json')

class ConvoyersView(APIView):
	def get(self, request, format=None):
		convoyId = self.request.query_params.get('convoy_id', None)
		convoyers = Convoyer.objects.filter(convoy=convoyId)		
		serializer = ConvoyerSerializer(convoyers, many=True)		
		data = JSONRenderer().render(serializer.data)
		return HttpResponse(data, content_type='application/json')

class HistorialView(APIView):
	def get(self, request, format=None):
		userId = self.request.query_params.get('user_id', None)
		convoy = Convoy.objects.filter(status=3).filter(user=userId)
		serializer = ConvoySerializer(convoy, many=True)
		data = JSONRenderer().render(serializer.data)
		return HttpResponse(data, content_type='application/json')






