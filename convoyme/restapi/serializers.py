from models import Convoy, Friend, Convoyer
from rest_framework import serializers
from django.contrib.auth.models import User


class UserSerializer2(serializers.ModelSerializer):
	class Meta:
		model = User
		fields = ('id', 'username', 'email', 'first_name', 'last_name')

class ConvoySerializer(serializers.ModelSerializer):
	class Meta:
		model = Convoy

class ConvoyerSerializer(serializers.ModelSerializer):
	User = UserSerializer2(source='user', many=False)
	class Meta:
		model = Convoyer
		fields = ('id', 'User', 'convoy', 'latitude', 'longitude', 'device_token','status')

class ConvoyerSerializer2(serializers.ModelSerializer):
	class Meta:
		model = Convoyer

class FriendSerializer(serializers.ModelSerializer):
	class Meta:
		model = Friend
		
class FriendSerializer2(serializers.ModelSerializer):
	User1 = UserSerializer2(source='user1', many=False)
	User2 = UserSerializer2(source='user2', many=False)
	class Meta:
		model = Friend
		fields = ('id', 'User1', 'User2', 'status')

# class UserSerializer2(serializers.ModelSerializer):
# 	class Meta:
# 		model = User
# 		fields = ('id', 'username', 'password', 'first_name', 'last_name', 'email')   
# 		extra_kwargs = {'password': {'read_only': True}}
	